package com.example.user.weatherapp;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;


@Dao
public interface PlacesDAO {

    @Query("SELECT * FROM places")
    List<ListPlaces> getAllPlaces();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertPlace(ListPlaces... places);

    @Query("DELETE  FROM places WHERE id = :placeId")
    abstract void deleteByUserId(long placeId);

//    UPDATE table_name
//    SET column1 = value1, column2 = value2, ...
//    WHERE condition;
    @Query("UPDATE  places SET day1 = :setDay1, day2 = :setDay2,day3 = :setDay3 WHERE id= :placeId")
    abstract void update(long placeId,String setDay1,String setDay2,String setDay3);

    @Query("SELECT * FROM places WHERE city = :checkCity ")
    List<ListPlaces>checkCity(String checkCity);
}

