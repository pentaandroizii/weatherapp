package com.example.user.weatherapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.user.weatherapp.forecastresult.ForecastResult;
import com.example.user.weatherapp.result.Result;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private Units UNIT = Units.METRIC;
    private static final String TAG = "MapsActivity";
    public final String API_KEY = "e7cfc1d9d47f1b384ca8b9f0972958b9";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private Boolean locationPermissionsGranted = false;
    private GoogleMap map;
    private Marker marker;
    private static String formattedFirstDay;
    private static String formattedSecondDay;
    private static String formattedThirdDay;
    private int shouldDisplayMarker = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getLocationPermission();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "Map is Ready", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onMapReady: map is ready");
        map = googleMap;
        if (locationPermissionsGranted) {
            getDeviceLocation();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            map.setMyLocationEnabled(true);
            map.getUiSettings().setMyLocationButtonEnabled(true);
            map.getUiSettings().setMapToolbarEnabled(false);
            MarkerInfoWindowAdapter markerInfoWindowAdapter = new MarkerInfoWindowAdapter(MapsActivity.this);
            map.setInfoWindowAdapter(markerInfoWindowAdapter);
        }
        addPin();
    }

    private void getDeviceLocation() {
        FusedLocationProviderClient fusedLocationProviderClient;
        Log.d(TAG, "getDeviceLocation: getting the devices current location");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            if (locationPermissionsGranted) {
                final Task location = fusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: found location!");
                            Location currentLocation = (Location) task.getResult();
                            try{

                                moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                        DEFAULT_ZOOM);
                                String city = getLocation(currentLocation.getLatitude(), currentLocation.getLongitude());
                                forecastRefresh(city);
                            }catch (Exception e){

                            }
                        } else {
                            Log.d(TAG, "onComplete: current location is null");
                            Toast.makeText(MapsActivity.this, "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void moveCamera(LatLng latLng, float zoom) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private void addPin() {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                map.clear();
                shouldDisplayMarker = 0;
                map.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                marker = map.addMarker(markerOptions);
                marker.setTitle("");
                try{
                    apiCall(latLng.latitude, latLng.longitude, marker);
                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),"Couldn't find weather!" , Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initMap() {
        Log.d(TAG, "initMap: initializing map");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapsActivity.this);
    }

    private void getLocationPermission() {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationPermissionsGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(this,
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    public String getLocation(double latitude, double longitude) {
        String city = "";
        Log.d(TAG, "geoLocate: geolocating");
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
            Address address = list.get(0);
            if (address.getLocality() != null) {
                city = address.getLocality();
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error: " + e, Toast.LENGTH_SHORT).show();
        }
        return city;
    }

    public void apiCall(final double latitude, final double longitude, final Marker markerOptions) {
        final String city = getLocation(latitude, longitude);
        Api api = ApiClient.getClient().create(Api.class);
        Call<Result> call = api.getCurrentWeather(city, API_KEY, UNIT );
        call.enqueue(new Callback<Result>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    Result results = (Result) response.body();
                    int temperature = (results.getMain().getTemp().intValue());
                    forecastRefresh(city);
                    String temp = marker.getTitle();
                    if(temp.length() > 0)
                        temp = "/" + temp;
                    marker.setTitle(  city + " : " + temperature + temp);
                    shouldDisplayMarker ++;
                    displayMarkerInfo();
                } catch (Exception ee) {
                    marker.setTitle("Unknown location");
                    marker.showInfoWindow();
                    ee.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Log.d("onFailure", "failed ApiCall");
            }
        });
    }

    private void displayMarkerInfo()
    {
        if(shouldDisplayMarker == 2) {
            marker.showInfoWindow();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void forecastRefresh(String city) {
        ForecastWeatherGet weatherGet = new ForecastWeatherGet(city, UNIT, this);
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    public void displayForecast(ForecastResult result) {
        int j = 0;
        try {
            double temperature;
            double arithmeticTempFirstDay;
            double arithmeticTempSecondDay;
            double arithmeticTempThirdDay;
            double sum = 0;
            int i = 0;
            boolean whileResult = true;

            @SuppressLint("SimpleDateFormat") SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
            Date d = new Date();
            String dayOfTheWeek = dayFormat.format(d);
            while (whileResult) {
                whileResult = result.getList().get(i).getDtTxt().substring(8, 10).contains(dayOfTheWeek);
                if (whileResult) {
                    i++;
                    j = i;
                }
            }
            for (i = j; i < j + 8; i++) {
                temperature = result.getList().get(i).getMain().getTemp();
                sum = sum + temperature;
            }
            j = i;
            arithmeticTempFirstDay = sum / 8;
            formattedFirstDay = String.format("%.0f", arithmeticTempFirstDay);
            sum = 0;

            for (i = j; i < j + 8; i++) {
                temperature = result.getList().get(i).getMain().getTemp();
                sum = sum + temperature;
            }
            arithmeticTempSecondDay = sum / 8;
            formattedSecondDay = String.format("%.0f", arithmeticTempSecondDay);
            sum = 0;
            j = i;

            for (i = j; i < j + 8; i++) {
                temperature = result.getList().get(i).getMain().getTemp();
                sum = sum + temperature;
            }
            arithmeticTempThirdDay = sum / 8;
            formattedThirdDay = String.format("%.0f", arithmeticTempThirdDay);
            marker.setTitle(marker.getTitle()+"/"+formattedFirstDay +"/" + formattedSecondDay + "/"+formattedThirdDay);
            shouldDisplayMarker++;
            displayMarkerInfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
