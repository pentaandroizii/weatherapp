package com.example.user.weatherapp;

import com.example.user.weatherapp.forecastresult.ForecastResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    //FORECAST
    @GET("forecast")

    Call<ForecastResult> getForecastWeather(@Query("q") String city, @Query("appid") String appid,@Query("units") String units);
    //api.openweathermap.org/data/2.5/forecast/daily?q=London&mode=xml&units=metric&cnt=7
    @GET("forecast")

    Call<ForecastResult> getForecastWeatherCoord(@Query("lat") Double lat,@Query("lon") Double lon, @Query("appid") String api, @Query("units") String units);
}