package com.example.user.weatherapp;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;


public class MyPlaces extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_places);

        recyclerView = findViewById(R.id.myPlacesList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        final PlacesDatabase placesDatabase = Room.databaseBuilder(getApplicationContext(), PlacesDatabase.class , "my places")
                .allowMainThreadQueries()
                .build();
        List<ListPlaces> places =  placesDatabase.placesDAO().getAllPlaces();
        adapter = new MyPlacesAdapter(places,this);
        recyclerView.setAdapter(adapter);
    }
    @Override
    public void onBackPressed() {
        startActivity(new Intent(this , WeatherActivity.class));
    }
}
