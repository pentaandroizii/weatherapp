package com.example.user.weatherapp;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyPlacesAdapter extends RecyclerView.Adapter<MyPlacesAdapter.ViewHolder>{

   private List<ListPlaces> listItems;
   private Context context;

    public MyPlacesAdapter(List<ListPlaces> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item,parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.city.setText(listItems.get(position).getCity());
        holder.day1.setText(listItems.get(position).getDay1());
        holder.day2.setText(listItems.get(position).getDay2());
        holder.day3.setText(listItems.get(position).getDay3());

        holder.temperatureCurrent.setText(listItems.get(position).getTemperatureCurrent());
        holder.temperatureDay1.setText(listItems.get(position).getTemperatureDay1());
        holder.temperatureDay2.setText(listItems.get(position).getTemperatureDay2());
        holder.temperatureDay3.setText(listItems.get(position).getTemperatureDay3());

        holder.prognoseCurrent.setText(listItems.get(position).getPrognoseCurrent());
        holder.prognoseDay1.setText(listItems.get(position).getPrognoseDay1());
        holder.prognoseDay2.setText(listItems.get(position).getPrognoseDay2());
        holder.prognoseDay3.setText(listItems.get(position).getPrognoseDay3());

        final PlacesDatabase placesDatabase = Room.databaseBuilder(context, PlacesDatabase.class , "my places")
                .allowMainThreadQueries()
                .build();

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(context);
                dialog.setMessage("Do you want to remove " + listItems.get(position).getCity() + " from My Places?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                placesDatabase.placesDAO().deleteByUserId(listItems.get(position).getId());
                                Intent intent = new Intent(context,MyPlaces.class);
                                context.startActivity(intent);
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                            }
                        });
                dialog.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView city;
        public TextView day1;
        public TextView day2;
        public TextView day3;

        public TextView temperatureCurrent;
        public TextView temperatureDay1;
        public TextView temperatureDay2;
        public TextView temperatureDay3;

        public TextView  prognoseCurrent;
        public TextView  prognoseDay1;
        public TextView  prognoseDay2;
        public TextView  prognoseDay3;

        public ImageView deleteButton;



        public ViewHolder(View itemView) {
            super(itemView);

            city = itemView.findViewById(R.id.my_places_city);
            day1 = itemView.findViewById(R.id.my_places_day1);
            day2 = itemView.findViewById(R.id.my_places_day2);
            day3 = itemView.findViewById(R.id.my_places_day3);

            temperatureCurrent = itemView.findViewById(R.id.my_places_temperature_current);
            temperatureDay1 = itemView.findViewById(R.id.my_places_temperature_day1);
            temperatureDay2 = itemView.findViewById(R.id.my_places_temperature_day2);
            temperatureDay3 = itemView.findViewById(R.id.my_places_temperature_day3);

            prognoseCurrent = itemView.findViewById(R.id.my_places_prognose_current);
            prognoseDay1 = itemView.findViewById(R.id.my_places_prognose_day1);
            prognoseDay2 = itemView.findViewById(R.id.my_places_prognose_day2);
            prognoseDay3 = itemView.findViewById(R.id.my_places_prognose_day3);

            deleteButton = itemView.findViewById(R.id.my_places_delete_button);
        }
    }



}


