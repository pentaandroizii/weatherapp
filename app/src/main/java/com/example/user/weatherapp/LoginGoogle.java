package com.example.user.weatherapp;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

public class LoginGoogle implements GoogleApiClient.OnConnectionFailedListener {
    private static final int REQ_CODE = 600;
    public void login(Activity activity) {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(activity).enableAutoManage((FragmentActivity) activity, this).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(activity);
        intent.putExtra("profile",account);
        activity.startActivityForResult(intent, REQ_CODE);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

/*
    public void signOut() {        loggedInGoogle=false;

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {

            @Override
            public void onResult(@NonNull Status status) {

            }
        });
    }
*/
}
