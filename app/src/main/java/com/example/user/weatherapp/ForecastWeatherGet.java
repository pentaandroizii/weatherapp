package com.example.user.weatherapp;

import android.content.Context;
import android.widget.Toast;

import com.example.user.weatherapp.forecastresult.ForecastResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForecastWeatherGet {
    static ForecastResult forecastResults;
    private static final String API_KEY = "e7cfc1d9d47f1b384ca8b9f0972958b9";

    public ForecastWeatherGet(String currentCity, Units unit, final Context context) {

        ApiInterface apiInterface = ApiSecondClient.getForecast().create(ApiInterface.class);
        Call<ForecastResult> call = apiInterface.getForecastWeather(currentCity, API_KEY, unit.name().toLowerCase());
        call.enqueue(new Callback<ForecastResult>() {

            @Override
            public void onResponse(Call<ForecastResult> call, Response<ForecastResult> response) {
                ForecastWeatherGet.forecastResults = response.body();
                if (context.getClass().equals(MapsActivity.class)) {
                    ((MapsActivity) context).displayForecast(forecastResults);
                } else {
                    ((WeatherActivity) context).displayForecast(forecastResults);
                }
            }

            @Override
            public void onFailure(Call<ForecastResult> call, Throwable t) {
                Toast.makeText(context, "Api call error", Toast.LENGTH_SHORT);
            }
        });
    }
}