package com.example.user.weatherapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    SignInButton signInButton;

    CallbackManager callBackManager;
    private LoginGoogle loginGoogle;
    public static final int REQ_CODE = 600;
    public static final String PREFERENCE = "preference";
    public static final String PREF_NAME = "name";
    public static final String PREF_URL = "url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FacebookSdk.sdkInitialize(getApplicationContext());
        loginGoogle = new LoginGoogle();

        callBackManager = CallbackManager.Factory.create();
        signInButton = findViewById(R.id.bn_login);
        signInButton.setOnClickListener(this);
        LoginButton loginButton = findViewById(R.id.login_button);

        Log.d("CHECKING" , "ON CREATE");
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
            }
        };

        ProfileTracker profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                nextActivity(newProfile);
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();

        FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Profile profile = Profile.getCurrentProfile();
                nextActivity(profile);
                Toast.makeText(getApplicationContext(), "Logging in...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException e) {
            }
        };
        loginButton.setReadPermissions("public_profile", "email");
        loginButton.registerCallback(callBackManager, callback);
    }
    private void nextActivity(Profile profile){
        Log.d("CHECKING" , "NEXT ACTIVITY");
        if(profile != null){
            SharedPreferences mSharedPreference = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = mSharedPreference.edit();
            mEditor.putString(PREF_NAME, profile.getFirstName()+ " " + profile.getLastName());
            mEditor.putString(PREF_URL, profile.getProfilePictureUri(80,80).toString());
            mEditor.apply();
            Intent intent = new Intent(this,WeatherActivity.class);
            startActivity(intent);
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bn_login:
                loginGoogle.login(Login.this);
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("CHECKING" , "ACTIVITY RESULT GOOGLE");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            getResult(result);
        } else {

            callBackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
    //    public void storeProfile(String a , String b){
//        SharedPreferences mSharedPreference = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
//        SharedPreferences.Editor mEditor = mSharedPreference.edit();
//    }
    public void getResult(GoogleSignInResult result) {
        Log.d("CHECKING" , "GET RESULT");
        if (result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            SharedPreferences mSharedPreference = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
            SharedPreferences.Editor mEditor = mSharedPreference.edit();
            mEditor.putString(PREF_NAME, account.getDisplayName());


            try{
                mEditor.putString(PREF_URL, account.getPhotoUrl().toString());
            }catch (NullPointerException e){
                e.printStackTrace();
            }
            startActivity(new Intent(this,WeatherActivity.class));
//            session.setLoggedin(true);
            mEditor.apply();
            finish();

        } else {
            Toast.makeText(getApplicationContext(), "Google Login Failed", Toast.LENGTH_LONG).show();
            finish();
            Intent intent = new Intent(this,Login.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
