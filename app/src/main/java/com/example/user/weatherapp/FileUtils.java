package com.example.user.weatherapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public final class FileUtils {
    private FileUtils() {
    }

    public static File saveImage(byte[] content,  Context context) throws IOException {

        String name ="profile.png";
        File storageDir = context.getDir("profilePicDir", context.MODE_PRIVATE);
        File image = new File(storageDir, name);
        FileOutputStream out = new FileOutputStream(image);
        try {
            out.write(content);
            out.close();
            Toast.makeText(context,"PHOTO Saved to : "+context.getDir("profilePicDir", context.MODE_PRIVATE) ,Toast.LENGTH_LONG);
        } catch (Exception e) {
            e.printStackTrace();
            out.close();
            Toast.makeText(context,"PHOTO ERROR SAVE FILE UTILS : "+context.getDir("profilePicDir", context.MODE_PRIVATE) ,Toast.LENGTH_LONG);

        }
        return image;
    }

    public static Bitmap getImageFromStorage(String path){
        Bitmap b = null;
        if (!("").equals(path.replace(" ", "")))
            try {

                File f = new File(path.substring(0, path.lastIndexOf('/')), path.substring(path.lastIndexOf('/') + 1));
                b = BitmapFactory.decodeStream(new FileInputStream(f));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        return b;
    }
}