package com.example.user.weatherapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.user.weatherapp.forecastresult.ForecastResult;
import com.example.user.weatherapp.result.Result;
import com.facebook.login.LoginManager;
import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.MagicalPermissions;
import com.frosquivel.magicalcamera.Utilities.ConvertSimpleImage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.user.weatherapp.Units.METRIC;

public class WeatherActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener {

    private TextView currentLocation;
    private SlidingDrawer slidingDrawer;
    private  boolean locationFound = false;
    private AutoCompleteTextView searchEditText;
    private Switch chooseSearchType;
    private Address address;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private NavigationView navigation;
    private CircleImageView profileImage;
    private int GALLERY = 1;
    private MagicalCamera camera;
    private Units UNIT = Units.METRIC;
    private String temperatureSymbol = "°C";
    private EditText latitude;
    private EditText longitude;

    String dbcity,dbcTemp,dbcProc,dbday1,dbday2,dbday3,dbtemp1,dbtemp2,dbtemp3,dbproc1,dbproc2,dbproc3;
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        initializeMenu();
        onCreateMethod();
        switchTemp();

//android:id="@+id/addToMyPlacesButton"
       addToMyPlaces();


    }

    private void addToMyPlaces(){
        final ImageView addToMyPlacesButton = findViewById(R.id.addToMyPlacesButton);
        final PlacesDatabase placesDatabase = Room.databaseBuilder(getApplicationContext(), PlacesDatabase.class , "my places")
                .allowMainThreadQueries()
                .build();

        addToMyPlacesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<ListPlaces> list = placesDatabase.placesDAO().checkCity(dbcity);
                list.size();

                if(list.size() == 0 && dbcity != null) {
                    addToMyPlacesButton.setVisibility(View.VISIBLE);
                    android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(WeatherActivity.this);
                    dialog.setMessage("Do you want to add " + dbcity + " to My Places?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                    placesDatabase.placesDAO().insertPlace(new ListPlaces(dbcity,dbday1,dbday2,dbday3,
                                            dbcTemp,dbtemp1,dbtemp2,dbtemp3,
                                            dbcProc,dbproc1,dbproc2,dbproc3));
                                    Toast.makeText(getApplicationContext(),dbcity + " saved to My Places.",Toast.LENGTH_SHORT).show();

                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                                }
                            });
                    dialog.show();
                } else{
                    if(dbcity == null){
                        Toast.makeText(getApplicationContext(), "Turn on internet.",Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(getApplicationContext(),dbcity + " already exist in My Places.",Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });

    }

    private void onCreateMethod() {
        geoLocateStart();
        getLocationPermission();
        init();
        setVisibility();
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        TextView dayOne = findViewById(R.id.firstDayDate);
        TextView dayTwo = findViewById(R.id.secondDayDate);
        TextView dayThree = findViewById(R.id.thirdDayDate);
        latitude = findViewById(R.id.latitude);
        longitude = findViewById(R.id.longitude);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM");

        Date dayOneForecast = new Date();
        dayOneForecast.setDate(dayOneForecast.getDate() + 1);

        String month = monthFormat.format(dayOneForecast);
        String dayOneOfTheWeek = dayFormat.format(dayOneForecast);

        Date dayTwoForecast = new Date();
        dayTwoForecast.setDate(dayTwoForecast.getDate() + 2);
        String dayTwoOfTheWeek = dayFormat.format(dayTwoForecast);

        Date dayThreeForecast = new Date();
        dayThreeForecast.setDate(dayThreeForecast.getDate() + 3);
        String monthTwo = monthFormat.format(dayThreeForecast);
        String dayThreeOfTheWeek = dayFormat.format(dayThreeForecast);

        dayOne.setText("Tomorrow");
        dayTwo.setText(month + " " + dayTwoOfTheWeek);
        dayThree.setText(monthTwo + " " + dayThreeOfTheWeek);
        dbday1 = month + " " + dayOneOfTheWeek;
        dbday2 = month + " " + dayTwoOfTheWeek;
        dbday3 = monthTwo + " " + dayThreeOfTheWeek;

        searchEditText = findViewById(R.id.search_edit);
        chooseSearchType = findViewById(R.id.switchButton);

        slidingDrawer = findViewById(R.id.SlidingDrawer);
        currentLocation = findViewById(R.id.currentLocationTextView);
        changeSlideButtonText();
        initAutoCompleteSearch();
        searchButtonClick();
        fuseLocation();

    }

    private void changeSlideButtonText()
    {   final Button slideButton;
        slideButton = findViewById(R.id.slideButton);
        slidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDrawerOpened() {
                slideButton.setText(">>Swipe up to cancel search<<");
            }
        });

        slidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDrawerClosed() {
                slideButton.setText(">>Swipe down to search location<<");
                searchEditText.setText("");
            }
        });


    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void forecastRefresh(String city) {
        ForecastWeatherGet weatherGet = new ForecastWeatherGet(city, UNIT, this);
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    public void displayForecast(ForecastResult result) {
        TextView firstDayDescription = findViewById(R.id.firstDayDescription);
        TextView secondDayDescription = findViewById(R.id.secondDayDescription);
        TextView thirdDayDescription = findViewById(R.id.thirdDayDescription);
        TextView firstDayTemperature = findViewById(R.id.firstDayTemperature);
        TextView secondDayTemperature = findViewById(R.id.secondDayTemperature);
        TextView thirdDayTemperature = findViewById(R.id.thirdDayTemperature);
        int j = 0;
        try {

            double temperature;
            double arithmeticTempFirstDay;
            double arithmeticTempSecondDay;
            double arithmeticTempThirdDay;
            double sum = 0;

            String formattedFirstDay;
            String formattedSecondDay;
            String formattedThirdDay;

            int i = 0;
            boolean whileResult = true;
            @SuppressLint("SimpleDateFormat") SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
            Date d = new Date();
            String dayOfTheWeek = dayFormat.format(d);
            while (whileResult) {
                whileResult = result.getList().get(i).getDtTxt().substring(8, 10).contains(dayOfTheWeek);
                if (whileResult) {
                    i++;
                    j = i;
                }
            }
            for (i = j; i < j + 8; i++) {
                temperature = result.getList().get(i).getMain().getTemp();
                sum = sum + temperature;
            }
            j = i;
            arithmeticTempFirstDay = sum / 8;
            sum = 0;
            for (i = j; i < j + 8; i++) {
                temperature = result.getList().get(i).getMain().getTemp();
                sum = sum + temperature;
            }
            arithmeticTempSecondDay = sum / 8;
            sum = 0;
            j = i;
            for (i = j; i < j + 8; i++) {
                temperature = result.getList().get(i).getMain().getTemp();
                sum = sum + temperature;
            }
            arithmeticTempThirdDay = sum / 8;
            if (UNIT == METRIC) {
                formattedThirdDay = String.format("%.0f", arithmeticTempThirdDay);
                thirdDayTemperature.setText(formattedThirdDay + "°");

                formattedSecondDay = String.format("%.0f", arithmeticTempSecondDay);
                secondDayTemperature.setText(formattedSecondDay + "°");

                formattedFirstDay = String.format("%.0f", arithmeticTempFirstDay);
                firstDayTemperature.setText(formattedFirstDay + "°");
            } else {
                formattedThirdDay = String.format("%.1f", arithmeticTempThirdDay);
                thirdDayTemperature.setText(formattedThirdDay + "°");

                formattedSecondDay = String.format("%.1f", arithmeticTempSecondDay);
                secondDayTemperature.setText(formattedSecondDay + "°");

                formattedFirstDay = String.format("%.1f", arithmeticTempFirstDay);
                firstDayTemperature.setText(formattedFirstDay + "°");
            }
            String firstDescription = result.getList().get(3).getWeather().get(0).getDescription();
            String secondDescription = result.getList().get(11).getWeather().get(0).getDescription();
            String thirdDescription = result.getList().get(19).getWeather().get(0).getDescription();

            firstDayDescription.setText(firstDescription);
            secondDayDescription.setText(secondDescription);
            thirdDayDescription.setText(thirdDescription);

            dbproc1 = firstDescription;
            dbproc2 = secondDescription;
            dbproc3 = thirdDescription;

            dbtemp1 = formattedFirstDay + "°";
            dbtemp2 = formattedSecondDay + "°";
            dbtemp3 = formattedThirdDay + "°";

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initAutoCompleteSearch() {
        PlaceAutocompleteAdapter placeAutocompleteAdapter;
        GoogleApiClient googleApiClient;
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build();
        final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-90, -180), new LatLng(90, 180));
        googleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        placeAutocompleteAdapter = new PlaceAutocompleteAdapter(this, googleApiClient, LAT_LNG_BOUNDS, typeFilter);
        searchEditText.setAdapter(placeAutocompleteAdapter);

        searchEditText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                collaspeKeyboard(view);
            }
        });

        searchEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    collaspeKeyboard(v);
                }
            }
        });
        onEditorListeners(searchEditText);
        onEditorListeners(latitude);
        onEditorListeners(longitude);
    }
    private void editorActionListener(int actionId, KeyEvent keyEvent, View v) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH
                || actionId == EditorInfo.IME_ACTION_DONE
                || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {
            collaspeKeyboard(v);
            performSearch();
        }
    }

    private void onEditorListeners(final EditText searchBy) {
        searchBy.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                editorActionListener(actionId, keyEvent, searchBy);
                return false;
            }
        });
    }

    private void toastIt(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, -140);
        toast.show();
    }

    private void setLayoutInvisible() {
        LinearLayout currentLinear = findViewById(R.id.currentLayout);
        currentLinear.setVisibility(View.INVISIBLE);
    }

    private void setLayoutVisible() {
        LinearLayout currentLinear = findViewById(R.id.currentLayout);
        currentLinear.setVisibility(View.VISIBLE);
    }

    private void fuseLocation() {
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(isInternetActive()) {
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                try {
                                    LocationApiCall(location.getLatitude(), location.getLongitude(), UNIT, temperatureSymbol);

                                } catch (Exception ee) {
                                    toastIt("Couldn't find location");
                                    setLayoutInvisible();
                                }
                            }
                        }
                    });
        }
        else
            toastIt("Please connect to internet");
    }

    private void searchButtonClick() {
        Button searchButton = findViewById(R.id.search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setLayoutVisible();
                performSearch();
                collaspeKeyboard(v);
            }
        });
    }

    private void performSearch() {
       if(isInternetActive())
            if (chooseSearchType.isChecked()) {
                geoLocateCoordinates();
                LocationApiCall(address.getLatitude(), address.getLongitude(), UNIT, temperatureSymbol);
            } else {
                geoLocate(searchEditText.getText().toString());
                LocationApiCall(address.getLatitude(), address.getLongitude(), UNIT, temperatureSymbol);
            }
       else
       {
           toastIt("Please connect to internet");
       }
        if (locationFound) {
            slidingDrawer.close();
        }
    }

    private void switchTemp(){
        final Switch temperatureSwitch;
         temperatureSwitch=findViewById(R.id.switchTemperature);
        temperatureSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                try{
                    if (b) {
                        UNIT = Units.IMPERIAL;
                        temperatureSymbol = "°F";
                        LocationApiCall(address.getLatitude(), address.getLongitude(), UNIT, temperatureSymbol);
                        temperatureSwitch.setText("Switch to: °C ");
                    } else {
                        temperatureSwitch.setText("Switch to: °F ");
                        UNIT = METRIC;
                        temperatureSymbol = "°C";
                        LocationApiCall(address.getLatitude(), address.getLongitude(), UNIT, temperatureSymbol);
                    }
                }
                catch(Exception e)
                {
                    toastIt("Please connect to internet");
                    e.printStackTrace();
                }
            }
        });
    }

    private void geoLocate(String city) {
        Geocoder geocoder = new Geocoder(WeatherActivity.this);
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocationName(city, 1);
        } catch (IOException e) {
            Log.e(getResources().getString(R.string.TAG), "geoLocate: IOException: " + e.getMessage());
        }
        if (list.size() > 0) {
            address = list.get(0);
            Log.d(getResources().getString(R.string.TAG), "geoLocate: found a location: " + address.toString());
            if (address.getLocality() == null) {
                toastIt("Location does not have a valid locality");
                locationFound = false;
            } else {
                currentLocation.setText(address.getLocality());
                dbcity= address.getLocality();
                locationFound = true;
            }
        } else {
            toastIt("Couldn't find location");
            setLayoutInvisible();
            locationFound = false;
        }
    }

    private void geoLocateStart() {
        Geocoder geocoder = new Geocoder(WeatherActivity.this);
        try {
            geocoder.getFromLocationName("New York", 1);
        } catch (IOException ignored) { }
    }

    private void getLocationPermission() {
        final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
        final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
        Log.d(getResources().getString(R.string.TAG), "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COURSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(this,
                                permissions,
                                getResources().getInteger(R.integer.LOCATION_PERMISSION_REQUEST_CODE));
                    }
        } else {
            ActivityCompat.requestPermissions(this,
                    permissions,
                    getResources().getInteger(R.integer.LOCATION_PERMISSION_REQUEST_CODE));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(getResources().getString(R.string.TAG), "onRequestPermissionsResult: called.");
        switch (requestCode) {
            case 1234: {
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            Log.d(getResources().getString(R.string.TAG), "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(getResources().getString(R.string.TAG), "onRequestPermissionsResult: permission granted");
                }
            }
        }
    }

    private void collaspeKeyboard(View view) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert in != null;
        in.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    private void setVisibility() {
        final TextView textSwitch = findViewById(R.id.textSwitch);
        final RelativeLayout blockSearchLocation = findViewById(R.id.searchBar_layout);
        final LinearLayout searchCoordinates = findViewById(R.id.searchLat);
        chooseSearchType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    blockSearchLocation.setVisibility(View.INVISIBLE);
                    searchEditText.setVisibility(View.INVISIBLE);
                    searchCoordinates.setVisibility(View.VISIBLE);
                    textSwitch.setText("Search by name");
                } else {
                    blockSearchLocation.setVisibility(View.VISIBLE);
                    searchEditText.setVisibility(View.VISIBLE);
                    searchCoordinates.setVisibility(View.INVISIBLE);
                    textSwitch.setText("Search by coordinates");
                    searchEditText.setText("");
                }
            }
        });
    }

    private void geoLocateCoordinates() {
        String latitudeString = latitude.getText().toString();
        String longitudeString = longitude.getText().toString();
        Geocoder geocoder = new Geocoder(WeatherActivity.this);
        List<Address> list = new ArrayList<>();
        if ((isNumeric(latitudeString)) || (isNumeric(longitudeString))) {
                toastIt("Invalid input");
        } else if ((Double.parseDouble(latitudeString) > 90.0) ||
                (Double.parseDouble(latitudeString) < -90.0) ||
                (Double.parseDouble(longitudeString) > 180.0) ||
                (Double.parseDouble(longitudeString) < -180)) {
                toastIt("Invalid coordinates");
        } else {
            try {
                list = geocoder.getFromLocation(Double.parseDouble(latitudeString), Double.parseDouble(longitudeString), 1);
            } catch (IOException e) {
                Log.e(getResources().getString(R.string.TAG), "geoLocate: IOException: " + e.getMessage());
            }
            if (list.size() > 0) {
                address = list.get(0);
                if (address.getLocality() == null) {
                    toastIt("That's not a locality");
                } else {
                    locationFound = true;
                    Log.d(getResources().getString(R.string.TAG), "geoCoordinates: found a location: " + address.toString());
                    currentLocation.setText(address.getLocality());
                    dbcity = address.getLocality();
                }
            } else {
                toastIt("Couldn't find location");
                setLayoutInvisible();
                locationFound = false;
            }
            latitude.setText("");
            longitude.setText("");
        }
    }

    private boolean isInternetActive()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).
                getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).
                        getState() == NetworkInfo.State.CONNECTED;
    }

    private boolean isNumeric(String s) {
        return s == null || !s.matches("-?\\d+(\\.\\d+)?");
    }

    private String getLocation(double latitude, double longitude) {
        String city = "";
        Log.d(getResources().getString(R.string.TAG), "geoLocate: geolocating");
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> list = geocoder.getFromLocation(latitude, longitude, 1);
            address = list.get(0);
            if (address.getLocality() != null) {
                locationFound = true;
                city = address.getLocality();
                currentLocation.setText(city);
                dbcity = city;
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error: " + e, Toast.LENGTH_SHORT).show();
        }
        return city;
    }

    private void LocationApiCall(double latitude, double longitude, Units unit, final String symbol) {
        final String city = getLocation(latitude, longitude);
        final TextView temperatureView = findViewById(R.id.temperatureTextView);
        final TextView weatherDesciption = findViewById(R.id.weatherDescriptionTextView);
        Api api = ApiClient.getClient().create(Api.class);
        Call<Result> call = api.getCurrentWeather(city,getResources().getString(R.string.Weather_API_Key) , unit);
        call.enqueue(new Callback<Result>() {
            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                temperatureView.setText("");
                weatherDesciption.setText("");
                setLayoutInvisible();
                toastIt("Couldn't find weather for this location");
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call call, Response response) {
                String temperature;
                String weather;
                try {
                    setLayoutVisible();
                    Result results = (Result) response.body();
                    temperature = (results.getMain().getTemp().toString());
                    weather = results.getWeather().get(0).getDescription();
                    temperatureView.setText((int) (Math.round(Double.parseDouble(temperature))) + "°C");
                    dbcTemp = (int) (Math.round(Double.parseDouble(temperature))) + "°C";
                    if(UNIT==Units.METRIC) {
                        temperatureView.setText((int) (Math.round(Double.parseDouble(temperature))) + symbol);
                    }else{
                        temperatureView.setText(temperature + symbol);
                    }
                    weatherDesciption.setText(weather);
                    dbcProc = weather;
                    forecastRefresh(city);
                } catch (Exception ee) {
                    temperatureView.setText("");
                    weatherDesciption.setText("");
                    setLayoutInvisible();
                    toastIt("Couldn't find weather for this city");
                }
            }
        });
    }

    @SuppressLint("ShowToast")
    private void clearSharePref(){
        SharedPreferences mSharedPreferences = getSharedPreferences(getResources().getString(R.string.PREFERENCE), Context.MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putString(getResources().getString(R.string.PREF_NAME),null);
        mEditor.putString(getResources().getString(R.string.PREF_URL),null);
        try{
            LoginManager.getInstance().logOut();
            finish();
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"Erorr : " ,Toast.LENGTH_SHORT);
        }
        mEditor.apply();
    }

    private void initializeMenu(){
        DrawerLayout drawerLayout = findViewById(R.id.drawerLayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigation = findViewById(R.id.navigationView);
        setNavigationHeader();
        setMenuItem(navigation);
    }

    private void setMenuItem(NavigationView nav){
        nav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.weather:
                        weather();
                        return true;
                    case R.id.weather_logout:
                        clearSharePref();
                        startActivity(new Intent(getApplicationContext(),SplashScreenActivity.class));
                        return true;
                    case R.id.weather_maps:
                        if(isInternetActive())
                            startActivity(new Intent(getApplicationContext(), MapsActivity.class));
                        else
                            toastIt("Please connect to internet");
                        return true;
                    case R.id.weather_places:
                        startActivity(new Intent(getApplicationContext(),MyPlaces.class));
                        return true;
                    case R.id.weather_profile:
                        startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                }
                return false;
            }
        });
    }

    @SuppressLint("CommitPrefEdits")
    private void setNavigationHeader(){
        SharedPreferences mSharedPreferences = getSharedPreferences(getResources().getString(R.string.PREFERENCE), Context.MODE_PRIVATE);
        View linearLayout = navigation.inflateHeaderView(R.layout.navigation_header);
        TextView name = linearLayout.findViewById(R.id.profileTextView);
        profileImage = linearLayout.findViewById(R.id.profile_image);
        name.setText( mSharedPreferences.getString(getResources().getString(R.string.PREF_NAME),"User"));
        Toast.makeText(getApplicationContext(),mSharedPreferences.getString(getResources().getString(R.string.PREF_NAME),""),Toast.LENGTH_LONG).show();
        Glide.with(this)
                .load(mSharedPreferences.getString(getResources().getString(R.string.PREF_URL),""))
                .apply(new RequestOptions()
                        .placeholder(R.drawable.profile)
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(profileImage);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chose_photo();
            }
        });
    }

    private void chose_photo() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Toast.makeText(getApplicationContext(), "Gallery", Toast.LENGTH_SHORT).show();
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                Toast.makeText(getApplicationContext(), "Cam", Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
        MagicalPermissions permissions = new MagicalPermissions(this, SplashScreenActivity.permissions);
        camera = new MagicalCamera(this,Integer.valueOf(getResources().getString(R.string.RESIZE_PHOTO_PIXELS_PERCENTAGE)), permissions);

    }

    private void takePhotoFromCamera() {
        MagicalPermissions magicalPermissions = new MagicalPermissions(this,SplashScreenActivity.permissions);
        camera = new MagicalCamera(this, Integer.valueOf(getResources().getString(R.string.RESIZE_PHOTO_PIXELS_PERCENTAGE)), magicalPermissions);
        camera.takePhoto();
    }

    public void saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.PNG, 30, bytes);
        try {
            FileUtils.saveImage(bytes.toByteArray(), getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void weather(){
        Intent intent = new Intent(this,WeatherActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return actionBarDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        camera.resultPhoto(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 20, stream);
                    saveImage(bitmap);

                    Toast.makeText(WeatherActivity.this, "Image set!", Toast.LENGTH_SHORT).show();
                    profileImage.setImageBitmap(FileUtils.getImageFromStorage(getApplicationContext().getDir("profilePicDir", MODE_PRIVATE).getPath() + "/profile.png"));

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(WeatherActivity.this, "Failed set image from gallery!", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == MagicalCamera.TAKE_PHOTO) {
            Bitmap myImage = camera.rotatePicture(camera.getPhoto(), MagicalCamera.ORIENTATION_ROTATE_90);

            byte[] bytesArray = ConvertSimpleImage.bitmapToBytes(myImage, MagicalCamera.PNG);

            try {
                profileImage.setImageBitmap(myImage);
                Toast.makeText(WeatherActivity.this, "Image Saved cam to context!", Toast.LENGTH_SHORT).show();
                FileUtils.saveImage(bytesArray, getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(WeatherActivity.this, "Image err Saving cam to context!", Toast.LENGTH_SHORT).show();
            }
        }
    }

}