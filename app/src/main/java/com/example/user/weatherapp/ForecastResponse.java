package com.example.user.weatherapp;

import com.example.user.weatherapp.forecastresult.ForecastResult;
import com.example.user.weatherapp.forecastresult.Main;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForecastResponse {

    @SerializedName("list")
    private List<ForecastResult> list;

    public List<ForecastResult> getList() {
        return  list;
    }

    @SerializedName("main")
    private Main main;

    public Main getMain() {
        return main;
    }
/*
    @SerializedName("dt_txt")
    private Dt_txt main;

    public Main getMain() {
        return main;
    }*/
}