package com.example.user.weatherapp;

import com.example.user.weatherapp.result.Main;
import com.example.user.weatherapp.result.Result;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResultResponse {
    @SerializedName("weather")
    private List<Result> weather;
    public List<Result> getWeather(){
        return weather;
    }

    @SerializedName("main")
    private Main main;

    public Main getMain() {
        return main;
    }

}
