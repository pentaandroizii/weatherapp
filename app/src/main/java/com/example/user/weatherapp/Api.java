package com.example.user.weatherapp;

import com.example.user.weatherapp.result.Result;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {
    //String BASE_URL="http://api.openweathermap.org/data/2.5/";
    @GET("weather")

    Call<Result> getCurrentWeather(@Query("q") String city, @Query("appid") String appid,@Query("units") Units units);

    @GET("forecast/daily")
    Call<Result> getForecastWeather(@Query("q") String city,@Query("mode") String mode, @Query("appid") String appid,@Query("units") String units,@Query("cnt") int cnt);
}
