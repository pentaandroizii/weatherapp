package com.example.user.weatherapp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiSecondClient {
    private static final String BASE_URL="http://api.openweathermap.org/data/2.5/";
    private static Retrofit secondRetrofit=null;

    public static Retrofit getForecast(){
        if(secondRetrofit==null){
            secondRetrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return secondRetrofit;
    }
}