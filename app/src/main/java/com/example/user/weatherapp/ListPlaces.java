package com.example.user.weatherapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "places")
public class ListPlaces {

    @PrimaryKey(autoGenerate = true)
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    @ColumnInfo(name = "city")
    private String city;

    @ColumnInfo(name = "day1")
    private String day1;
    @ColumnInfo(name = "day2")
    private String day2;
    @ColumnInfo(name = "day3")
    private String day3;

    @ColumnInfo(name = "temperature_current")
    private String temperatureCurrent;
    @ColumnInfo(name = "temperature_day1")
    private String temperatureDay1;
    @ColumnInfo(name = "temperature_day2")
    private String temperatureDay2;
    @ColumnInfo(name = "temperature_day3")
    private String temperatureDay3;

    @ColumnInfo(name = "prognose_current")
    private String prognoseCurrent;
    @ColumnInfo(name = "prognose_day1")
    private String prognoseDay1;
    @ColumnInfo(name = "prognose_day2")
    private String prognoseDay2;
    @ColumnInfo(name = "prognose_day3")
    private String prognoseDay3;


    public ListPlaces(String city, String day1, String day2, String day3,
                      String temperatureCurrent, String temperatureDay1, String temperatureDay2, String temperatureDay3,
                      String prognoseCurrent, String prognoseDay1, String prognoseDay2, String prognoseDay3) {
        this.city = city;
        this.day1 = day1;
        this.day2 = day2;
        this.day3 = day3;
        this.temperatureCurrent = temperatureCurrent;
        this.temperatureDay1 = temperatureDay1;
        this.temperatureDay2 = temperatureDay2;
        this.temperatureDay3 = temperatureDay3;
        this.prognoseCurrent = prognoseCurrent;
        this.prognoseDay1 = prognoseDay1;
        this.prognoseDay2 = prognoseDay2;
        this.prognoseDay3 = prognoseDay3;
    }

    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDay1() {
        return day1;
    }


    public String getDay2() {
        return day2;
    }


    public String getDay3() {
        return day3;
    }


    public String getTemperatureCurrent() {
        return temperatureCurrent;
    }

    public void setTemperatureCurrent(String temperatureCurrent) {
        this.temperatureCurrent = temperatureCurrent;
    }

    public String getTemperatureDay1() {
        return temperatureDay1;
    }

    public void setTemperatureDay1(String temperatureDay1) {
        this.temperatureDay1 = temperatureDay1;
    }

    public String getTemperatureDay2() {
        return temperatureDay2;
    }

    public void setTemperatureDay2(String temperatureDay2) {
        this.temperatureDay2 = temperatureDay2;
    }

    public String getTemperatureDay3() {
        return temperatureDay3;
    }

    public void setTemperatureDay3(String temperatureDay3) {
        this.temperatureDay3 = temperatureDay3;
    }

    public String getPrognoseCurrent() {
        return prognoseCurrent;
    }

    public void setPrognoseCurrent(String prognoseCurrent) {
        this.prognoseCurrent = prognoseCurrent;
    }

    public String getPrognoseDay1() {
        return prognoseDay1;
    }

    public void setPrognoseDay1(String prognoseDay1) {
        this.prognoseDay1 = prognoseDay1;
    }

    public String getPrognoseDay2() {
        return prognoseDay2;
    }

    public void setPrognoseDay2(String prognoseDay2) {
        this.prognoseDay2 = prognoseDay2;
    }

    public String getPrognoseDay3() {
        return prognoseDay3;
    }

    public void setPrognoseDay3(String prognoseDay3) {
        this.prognoseDay3 = prognoseDay3;
    }
}
