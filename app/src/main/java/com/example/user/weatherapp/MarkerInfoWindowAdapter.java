package com.example.user.weatherapp;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
    private Context context;
    private View markerView;
    public MarkerInfoWindowAdapter(Context context) {
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker arg0) {


        return null;
    }

    @Override
    public View getInfoContents(Marker arg0) {
        markerView = ((MapsActivity)context).getLayoutInflater().inflate(R.layout.map_marker_info_window, null);


        String[] title = arg0.getTitle().split("/");
        if (title.length == 4) {
            TextView currentWeather = markerView.findViewById(R.id.current_weather);

            currentWeather.setText(title[0] + "°C");
            TextView firstForecast = markerView.findViewById(R.id.first_forecast);
            TextView secondForecast = markerView.findViewById(R.id.second_forecast);
            TextView thirdForecast = markerView.findViewById(R.id.third_forecast);
            firstForecast.setText(title[1] + "°C");
            secondForecast.setText(title[2]+ "°C");
            thirdForecast.setText(title[3]+ "°C");

            return markerView;
        }
        return null;
    }
}